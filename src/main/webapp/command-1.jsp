<%@ page contentType="text/html; charset=UTF-8" %>
<html>
<head>
    <meta charset="UTF-8"/>
    <title>命令执行（无回显）</title>
</head>
<body>
<h1>命令执行（无回显）</h1>

<%
    String querystring = "?cmd=cp+/etc/passwd+/tmp/";
    String cmd = request.getParameter("cmd");
    if (cmd != null) {
        try {
            Runtime.getRuntime().exec(cmd);
        } catch (Exception e) {
            out.print("<pre>");
            e.printStackTrace(response.getWriter());
            out.print("</pre>");
        }
    }
%>
<p>Linux/MacOS 触发: </p>
<p>curl '<a href="<%=request.getRequestURL()+querystring%>" target="_blank"><%=request.getRequestURL() + querystring%>
</a>'</p>
<p>然后检查 /tmp 是否存在 passwd 这个文件</p>
<br>

</body>
</html>
