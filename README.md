# jrasp-vulns


## 简介

jrasp-vulns 是测试漏洞的模块

## 环境

+ 工程编译环境jdk8

+ war包运行环境 jdk6～11

## 使用

+ 进入工程目录之后执行 `mvn clean package`

+ 将 target 目录下的`jrasp-vulns.war` 复制 tomcat或者jetty的`webapps`下

+ 启动web容器，打开链接 `http://localhost:8080/jrasp-vulns/`


![img.png](img.png)

